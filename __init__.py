#GNU General Public License Version 3

bl_info = {
    "name" : "Light Manager",
    "author" : "Antipov Alexander",
    "description" : "Simple manager for lights",
    "blender" : (2, 90, 0),
    "location" : "View3D",
    "Version" : (0, 1, 0),
    "wiki_url": "https://gitlab.com/AntipovAE/light-manager/-/wikis/Light-Mananager-Wiki",
    "category": "Lighting"
}

import bpy
from bpy_extras.io_utils import ExportHelper, ImportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator
import json


class CreatePanel(bpy.types.Panel):
    bl_label = "Create panel"
    bl_idname = "LM_CreatePanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Light Manager'
    
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        
        row.operator("object.light_add", icon = 'LIGHT_SPOT', text = "Spot Light").type = 'SPOT'
        row.operator("object.light_add", icon = 'LIGHT_AREA', text = "Area Light").type = 'AREA'
        row = layout.row()
        row.operator("object.light_add", icon = 'LIGHT_SUN', text = "Sun Light").type = 'SUN'
        row.operator("object.light_add", icon = 'LIGHT_POINT', text = "Point Light").type = 'POINT'
        # TODO Make edit panel for world light
        #row = layout.row()
        #row.operator("world.new", icon = 'WORLD', text = "World")
    
        
class EditPanel(bpy.types.Panel):
    bl_label = "Edit panel"
    bl_idname = "LM_EditPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Light Manager'
    
    bl_options = {'DEFAULT_CLOSED'}
    
    
    def draw(self, context):
        scn = context.scene
        
        layout = self.layout
        
        box = layout.box()
        
        row = box.row()
        row = box.row()
        
        row.operator("lm.import_window", icon = 'IMPORT', text = "Import")
        row.operator("lm.export_window", icon = 'EXPORT', text = "Export")
        row = box.row()
        
        layout.separator()
        
        row = layout.row()
            
        scene = bpy.context.scene
        row.prop(scene.render, "engine")
        if scene.render.engine == "CYCLES":
            row = layout.row()
            row.prop(scene.cycles, "feature_set")
            row = layout.row()
            row.prop(scene.cycles, "device")

        layout.separator()
        box = layout.box()

        # List of lights
        row = box.row()
        row.alignment = 'CENTER'
        for element in bpy.context.scene.objects:
            if element.type != "LIGHT": continue

            box1 = box.box()

            col = box1.column(align=True)

            split = col.split(factor=0.2)
            col_1 = split.column()
            col_2 = split.column()
            col_3 = split.column()
            col_4 = split.column()
            col_5 = split.column()

            if element.select_get():
                col_1.alert = True
            col_1.operator("lm.select_light", icon='RESTRICT_SELECT_ON', text="").prop_string = element.name
            col_2.prop(element, "hide_viewport", icon='HIDE_OFF', text="")
            col_3.operator("lm.as_camera", icon='VIEW_CAMERA', text="").prop_string = element.name
            col_4.prop(bpy.context.space_data, "lock_camera", icon='LOCKED', text="")
            col_5.operator("lm.delete_light", icon='TRASH', text="").prop_string = element.name

            row = box1.row()
            if element.select_get():
                row.alert = True
            row.prop(element, "name")
            row.prop(element.data, "type", icon='LIGHT_' + element.data.type, text="")
            row = box1.row()
        
            #Edit panel for EEVEE render
            if scene.render.engine == "BLENDER_EEVEE":
                
                if element.show_light_subpanel:
                    row.prop(element, "show_light_subpanel", icon="TRIA_DOWN", text="")
                else:
                    row.prop(element, "show_light_subpanel", icon="RIGHTARROW_THIN", text="")
                
                row.prop(element.data, "color", text = "")
                # TODO Temperature of light
                row.prop(element.data, "energy")
                row.prop(element.data, "use_shadow", icon="SHADING_RENDERED", text = "")
                
                #Sub panel for EEVEE light
                   
                if element.show_light_subpanel:
                    row = box1.row()    
                    row.prop(element.data, "shadow_soft_size", text = "Size")
                    row.prop(element.data, "specular_factor", text = "Specular")
                    row = box1.row()
                    if element.data.type == 'SPOT':
                        row.prop(element.data, "spot_size", text="Spot size")
                    
        
        
            #Edit panel for CYCLES render
            if scene.render.engine == "CYCLES":
                if element.show_light_subpanel:
                    row.prop(element, "show_light_subpanel", icon="TRIA_DOWN", text="")
                else:
                    row.prop(element, "show_light_subpanel", icon="RIGHTARROW_THIN", text="")

                row.prop(element.data, "color", text="")
                # TODO Temperature of light
                row.prop(element.data, "energy")
                row.prop(element.data.cycles, "cast_shadow", icon="SHADING_RENDERED", text = "")

                # Sub panel for CYCLES light

                if element.show_light_subpanel:
                    row = box1.row()
                    row.prop(element.data, "shadow_soft_size", text = "Size")
                    row = box1.row()
                    if element.data.type == 'SPOT':
                        row.prop(element.data, "spot_size", text="Spot size")
                    # TODO "Use nodes" for lights
                    #row.prop(element.data, "use_nodes", icon="NODETREE", text="Use nodes")

                    #if element.data.use_nodes == True:
                        #row = box1.row()
                        #row.prop(element.data, "shadow_soft_size", text="Size")
                    #else:
                        #pass


class InfoPanel(bpy.types.Panel):
    bl_label = "Info panel"
    bl_idname = "LM_InfoPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Light Manager'

    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Visit gitlab.com/AntipovAE/light-manager for more information.")
        row = layout.row()
        link_button = row.operator('wm.url_open', icon="COMMUNITY", text='Wiki')
        link_button.url = 'https://gitlab.com/AntipovAE/light-manager/-/wikis/Light-Mananager-Wiki'


class AsCamera(bpy.types.Operator):
    bl_idname = "lm.as_camera"
    bl_label = "Object as camera"

    prop_string = bpy.props.StringProperty(name="String Value")

    def execute(self, context):
        message = (self.prop_string)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[message].select_set(True)
        bpy.context.view_layer.objects.active = bpy.data.objects[message]
        bpy.ops.view3d.object_as_camera()

        return {'FINISHED'}


#Delete operator
class DeleteLight(bpy.types.Operator):
    bl_idname = "lm.delete_light"
    bl_label = "Light Delete"

    prop_string = bpy.props.StringProperty(name="String Value")


    def execute(self, context):


        message = (self.prop_string)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[message].select_set(True)
        bpy.context.view_layer.objects.active = bpy.data.objects[message]
        bpy.ops.object.delete(use_global=False, confirm=False)

        return {'FINISHED'}

#Select operator
class SelectLight(bpy.types.Operator):
    bl_idname = "lm.select_light"
    bl_label = "Light Select"

    prop_string = bpy.props.StringProperty(name="String Value")


    def execute(self, context):


        message = (self.prop_string)
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[message].select_set(True)
        bpy.context.view_layer.objects.active = bpy.data.objects[message]

        return {'FINISHED'}

#Import Window
class ImportWindow(Operator, ExportHelper):
    bl_idname = "lm.import_window"
    bl_label = "Import Lights"

    # ImportHelper mixin class uses this
    filename_ext = ".json"

    filter_glob: StringProperty(
        default="*.json",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )
    def execute(self, context):
        with open(self.filepath) as json_file:
            self.import_light_data = json.load(json_file)

        for k in self.import_light_data:

            light_data = bpy.data.lights.new(name=k['nameData'], type=k['type'])
            light_data.energy = k['energy']
            light_data.color = (k['color'])
            light_data.use_shadow = (k['shadowEEVEE'])
            light_data.cycles.cast_shadow = (k['shadowCYCLES'])
            light_data.shadow_soft_size = (k['size'])
            light_data.specular_factor = (k['specular'])
            if light_data.type == 'SPOT':
                light_data.spot_size = (k['spotSize'])
            else:
                pass

            light_object = bpy.data.objects.new(name=k['name'], object_data=light_data)

            bpy.context.collection.objects.link(light_object)

            light_object.location = (k['location'])
            light_object.rotation_euler = (k['rotation'])
            light_object.scale = (k['scale'])

        return {'FINISHED'}

#Export Window
class ExportWindow(Operator, ExportHelper):
    bl_idname = "lm.export_window"
    bl_label = "Export Lights"

    # ExportHelper mixin class uses this
    filename_ext = ".json"

    filter_glob: StringProperty(
        default="*.json",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )
    
    
    def execute(self, context):
        self.dictionary = []

        for element in bpy.context.scene.objects:
            if element.type != "LIGHT": continue

            if element.data.type == 'SPOT':
                transform = {
                    'name': str(element.name),
                    'nameData': str(element.data.name),
                    'type': str(element.data.type),
                    'location': list(element.location),
                    'rotation': list(element.rotation_euler),
                    'scale': list(element.scale),
                    'color': list(element.data.color),
                    'energy': float(element.data.energy),
                    'shadowEEVEE': bool(element.data.use_shadow),
                    'shadowCYCLES': bool(element.data.cycles.cast_shadow),
                    'size': float(element.data.shadow_soft_size),
                    'specular': float(element.data.specular_factor),
                    'spotSize': float(element.data.spot_size)
                    }
            else:
                transform = {
                    'name': str(element.name),
                    'nameData': str(element.data.name),
                    'type': str(element.data.type),
                    'location': list(element.location),
                    'rotation': list(element.rotation_euler),
                    'scale': list(element.scale),
                    'color': list(element.data.color),
                    'energy': float(element.data.energy),
                    'shadowEEVEE': bool(element.data.use_shadow),
                    'shadowCYCLES': bool(element.data.cycles.cast_shadow),
                    'size': float(element.data.shadow_soft_size),
                    'specular': float(element.data.specular_factor)
                }

            self.dictionary.append(transform)
            
        with open(self.filepath, "w") as outfile:
            json.dump(self.dictionary, outfile, indent = 4)
        
        return {'FINISHED'}



def register():

    bpy.utils.register_class(CreatePanel)
    bpy.utils.register_class(EditPanel)
    bpy.utils.register_class(InfoPanel)
    bpy.utils.register_class(AsCamera)
    bpy.utils.register_class(DeleteLight)
    bpy.utils.register_class(SelectLight)
    bpy.utils.register_class(ImportWindow)
    bpy.utils.register_class(ExportWindow)

    bpy.types.Object.show_light_subpanel = bpy.props.BoolProperty(name='Show light sub panel', default=False)





def unregister():

    bpy.utils.unregister_class(CreatePanel)
    bpy.utils.unregister_class(EditPanel)
    bpy.utils.unregister_class(InfoPanel)
    bpy.utils.unregister_class(AsCamera)
    bpy.utils.unregister_class(DeleteLight)
    bpy.utils.unregister_class(SelectLight)
    bpy.utils.unregister_class(ImportWindow)
    bpy.utils.unregister_class(ExportWindow)

    del bpy.types.Object.show_light_subpanel



if __name__ == "__main__":
    register()
