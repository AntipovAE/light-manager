# Light manager for Blender

Simple light manager for Blender 2.80 and above.

Works with EEVEE and Cycles.

Can create four types of light. Import/Export lights in json format. Helps navigate lights and edit their parametrs.

# Install

1. Download .zip file
2. Go to Blender -> Edit -> Preferences -> Add-ons -> Install
3. Select dowloaded .zip file
4. Now you can see panel "Light Manager" in 3D View

# Documentation

https://github.com/Antipov-AE/light-manager.wiki.git

# License

GNU GENERAL PUBLIC LICENSE Version 3

https://github.com/Antipov-AE/light-manager/blob/master/LICENSE

